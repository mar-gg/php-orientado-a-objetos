<?php
include_once('transporte.php');

class Tren extends Transporte
{
    private $cantidad_vagones;

    public function __construct($nom, $vel, $com, $vagones)
    {
        parent::__construct($nom, $vel, $com);
        $this->cantidad_vagones = $vagones;
    }

    public function resumenTren()
    {
        $mensaje = parent::crear_ficha();
        $mensaje .= '<tr>
                    <td>Cantidad de Vagones:</td>
                    <td>' . $this->cantidad_vagones . '</td>
                </tr>';
        return $mensaje;
    }
}
?>
