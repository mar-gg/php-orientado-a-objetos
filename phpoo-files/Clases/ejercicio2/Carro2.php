<?php
//creación de la clase carro
class Carro2{
    //declaracion de propiedades
    public $color;
    public $modelo;
    private $resultadoVerificacion;

    //declaracion del método verificación
    public function verificacion($anio_fabricacion){
        // determinar el estado del vehículo según el año de fabricación
        if ($anio_fabricacion < 1990) {
            $this->resultadoVerificacion = 'No';
        } elseif ($anio_fabricacion >= 1990 && $anio_fabricacion <= 2010) {
            $this->resultadoVerificacion = 'Revisión';
        } else {
            $this->resultadoVerificacion = 'Sí';
        }
    }

    // obtener el resultado de la verificación
    public function getResultadoVerificacion(){
        return $this->resultadoVerificacion;
    }
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
    $Carro1->color = $_POST['color'];
    $Carro1->modelo = $_POST['modelo'];
    // Agregar el año de fabricación al método de verificación
    $Carro1->verificacion($_POST['anio_fabricacion']);
}
?>





