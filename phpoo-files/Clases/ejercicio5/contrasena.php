<?php

class Contraseña {
    private $nombre;
    private $contrasena;

    public function __construct($nombre_front) {
        $this->nombre = $nombre_front;
        $this->generarContrasena();
        echo $this->mostrar();
    }

    private function generarContrasena() {
        // generar una contraseña de 4 letras mayúsculas
        $this->contrasena = strtoupper(substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 4));
    }

    public function mostrar() {
        return 'Hola ' . $this->nombre . ', esta es tu contraseña: ' . $this->contrasena;
    }

    public function __destruct() {
        echo 'La contraseña ha sido destruida';
    }
}

$mensaje = '';

if (!empty($_POST)) {
    $contrasena1 = new Contraseña($_POST['nombre']);
}
?>
